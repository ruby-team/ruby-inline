Source: ruby-inline
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Patrick Ringl <patrick_@freenet.de>,
           Paul van Tilburg <paulvt@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               rake,
               ruby-minitest,
               ruby-zentest
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-inline.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-inline
Homepage: https://github.com/seattlerb/rubyinline
XS-Ruby-Versions: all

Package: ruby-inline
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: rake,
         ruby | ruby-interpreter,
         ruby-all-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: gcc | c-compiler
Description: Ruby library for embedding C/C++ external module code
 Inline allows you to write foreign code within your ruby code. It
 automatically determines if the code in question has changed and
 builds it only when necessary. The extensions are then automatically
 loaded into the class/module that defines it.
 .
 You can even write extra builders that will allow you to write inlined
 code in any language. Use Inline::C as a template and look at
 Module#inline for the required API.
