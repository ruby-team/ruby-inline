#!/usr/bin/ruby -w

require 'inline'

class Hello
  inline do |builder|
    builder.include "<stdio.h>"
    builder.c 'void hello() { puts("hello world"); }'
  end
end

Hello.new.hello
